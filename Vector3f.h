
#pragma once

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

/*!
 * \class Vector3f
 * 
 * \brief A class for handling a 3 dimensions vector.
 * 
 * That class can be used to represent a vector or a point as well, every time a 3 coordinate
 * object needs to be stored.
 * Each coordinate is a floating point value.
 * 
 * The class also defines standard normalized vectors for unit vectors on the three axis either for
 * positive value or for negative value.
 * 
 */
class Vector3f
{
	public:
		static Vector3f UP;
		static Vector3f DOWN;
		static Vector3f FORWARD;
		static Vector3f BACKWARD;
		static Vector3f RIGHT;
		static Vector3f LEFT;

		/*!
		 * \fn Vector3f()
		 * 
		 * \brief Default constructor.
		 * 
		 * That constructor can be used to create a single vector with each of its
		 * coordinates set to 0.0
		 * 
		 */
		Vector3f();
		
		/*!
		 * \fn Vector3f(float x, float y, float z)
		 * 
		 * \brief Builds a vector based on the specified coordinates.
		 * 
		 * That constructor can be used to build a Vector3f variable with arbitrary coordinates.
		 * 
		 */
		Vector3f(float x, float y, float z);
		
		/*!
		 * \fn Vector3f(Vector3f q)
		 *
		 * \brief Builds a vector from another vector.
		 *
		 * That function simply create an independant copy of another vector.
		 *
		 */
		 Vector3f(Vector3f q);
		
		/*!
		 * \fn =(Vector3f q)
		 * 
		 */
		Vector3f operator =(Vector3f q);
		
		// Scalar assignement operator
		Vector3f operator =(float v);
		
		// Equality operator
		bool operator==(const Vector3f& v);
		
		// Addition and assignement
		Vector3f operator +=(Vector3f q);
		
		// Simple addition
		Vector3f operator +(Vector3f q);
		
		// Substraction
		Vector3f operator -(Vector3f q);
		
		// Division by a scalar
		Vector3f operator /(float q);
		
		// Multiplication by a scalar
		Vector3f operator *(float q);
		
		// Compute and return vector length
		float length();
		
		char* print();
		
		// Returns a string for display for example
		String toString();
		
		// Set vector length to unity
		void Normalize();


		float x_;
		float y_;
		float z_;	
};

