#include "Vector3f.h"


/*
   Axes pré-définis (repère direct main droite)


         (Z)
          ^
          |
          |
          |
          |
          |
          |
          |
          |
          +----------------------------> (Y)
         /
        /
       /
      /
     /
    /
   v
  (X)
  
*/
Vector3f Vector3f::FORWARD 	= Vector3f(1, 0, 0);	// vecteur « Forward » => +X
Vector3f Vector3f::BACKWARD = Vector3f(-1, 0, 0);	// vecteur « Backward » => -X
Vector3f Vector3f::UP 		= Vector3f(0, 0, 1);	// vecteur « Up » => +Z
Vector3f Vector3f::DOWN 	= Vector3f(0, 0, -1);	// vecteur « Down » => -Z
Vector3f Vector3f::LEFT 	= Vector3f(0, 1, 0);	// vecteur « Left » => +Y
Vector3f Vector3f::RIGHT 	= Vector3f(0, -1, 0);	// vecteur « Right » => -Y


Vector3f::Vector3f()
{
	x_ = 0.0f;
	y_ = 0.0f;
	z_ = 0.0f;
}

Vector3f::Vector3f(float x, float y, float z)
{
	x_ = x;
	y_ = y;
	z_ = z;
}

Vector3f::Vector3f(Vector3f q)
{
	x_ = q.x_;
	y_ = q.y_;
	z_ = q.z_;
}

Vector3f Vector3f::operator =(Vector3f q)
{
	x_ = q.x_;
	y_ = q.y_;
	z_ = q.z_;
}


Vector3f Vector3f::operator =(float v)
{
	x_ = v;
	y_ = v;
	z_ = v;
}

bool Vector3f::operator ==(const Vector3f& v)
{
	if(fabsf(v.x_ - x_) <= 1e-6 && fabs(v.y_ - y_) <= 1e-6 && fabs(v.z_ - z_) <= 1e-6)
		return(true);
	else
		return(false);
}

Vector3f Vector3f::operator -(Vector3f q)
{
	return(Vector3f(x_ - q.x_, y_ - q.y_, z_ - q.z_));
}

Vector3f Vector3f::operator +(Vector3f q)
{
	return(Vector3f(x_ + q.x_, y_ + q.y_, z_ + q.z_));
}

Vector3f Vector3f::operator +=(Vector3f q)
{
	return(Vector3f(x_ + q.x_, y_ + q.y_, z_ + q.z_));
}

Vector3f Vector3f::operator /(float q)
{
	return(Vector3f(x_ / q, y_ / q, z_ / q));
}

Vector3f Vector3f::operator *(float q)
{
	return(Vector3f(x_ * q, y_ * q, z_ * q));
}

float Vector3f::length()
{
	return(sqrt(x_ * x_ + y_ * y_ + z_ * z_));
}

char* Vector3f::print()
{
	String mystr = String("(") + String(x_) + String(" ; ") + String(y_) + String(" ; ") + String(z_) + String(")");
	return((char*)mystr.c_str());
}

String Vector3f::toString()
{
	return(String("(") + String(x_) + String(" ; ") + String(y_) + String(" ; ") + String(z_) + String(")").c_str());
}

void Vector3f::Normalize()
{
	float l = sqrt(x_ * x_ + y_ * y_ + z_ * z_);
	if(!isnan(l) && fabsf(l) >= 1e-9)
	{
		x_ /= l;
		y_ /= l;
		z_ /= l;
	}
}






